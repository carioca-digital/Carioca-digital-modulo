import Carta from "./Carta.js"
export default class Mazo {

    constructor(scene) {
        this.mazo = []
        this.scene = scene;

    }

    generarMazo() {
        for (let i = 0; i < 2; i++) {
            for (const palo of Carta.PALOS) {
                for (const valor of Carta.VALORES) {
                    let carta = new Carta(this.scene, 0, 0, palo, valor);
                    this.mazo.push(carta);
                }
            }
            this.mazo.push(new Carta(this.scene, 0, 0, "JKR", ""));
            this.mazo.push(new Carta(this.scene, 0, 0, "JKR", ""));

        }
        return this;
    }

    /**
     *
     * @return devuelve el numero de cartas que hay en el mazo
     */
    numeroDeCartas() {
        return mazo.length;
    }

    /**
     * Este metodó se usa cuando se saca una carta del mazo
     * @return el método retorna carta que se encuentra en la primera posición y a la vez la elimina del mazo
     */
    sacarCarta() {

        return this.mazo.shift();
    }

    /**
     *
     * @param nroCartas Número de cartas a sacar del mazo
     * @return devuelve un ArrayList de cartas con un numero de cartas sacadas del mazo
     */
    sacarUnNumeroDeCartas(nroCartas) {
        let arrayCartas = [];
        for (let i = 0; i < nroCartas; i++) {
            arrayCartas.push(this.mazo.shift());

        }
        return arrayCartas;

    }

    revolverMazo() {
        this.mazo = this.mazo.sort((a, b) => 0.5 - Math.random());
        return this;
    }


}
