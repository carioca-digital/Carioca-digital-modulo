
export default class Carta extends Phaser.GameObjects.Sprite{
       /**
     * Son los simbolos de los palos de los naipes ingleses pica, diamante, trebol, corazon
     */
        static PALOS = ["S", "D", "C", "H"];

       /**
         * Son los valores de cada carta
         */
    
        static VALORES = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"];
    constructor(scene, x, y, palo,valor){
        let type = `carta${valor}${palo}`;
        super(scene,x,y,type);
        this.valor = valor;
        this.palo = palo;
        this.setScale(0.15,0.15);
        this.setInteractive();
        scene.input.setDraggable(this);


    }
    
    setPosition(x,y) {
      this.x=x;
      this.y=y;
    }
    
}