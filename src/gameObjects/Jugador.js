import Carta from "./Carta.js"
export default class Jugador {

    constructor(nombre){
        this.nombre = nombre;
        this.puntaje = 0;
        this.bajoSusCarta = false;
        this.yaSacoCarta = false;
        this.mano = [];
    }
    
}