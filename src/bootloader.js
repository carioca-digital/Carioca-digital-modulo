import Carta from "./gameObjects/Carta.js";
class Bootloader extends Phaser.Scene{
    constructor(){
        super({
            key: "Bootloader"
        })
    }

    preload(){
        this.load.on("complete", ()=>{
            this.scene.start("Scene_play");
        })// Se ejecuta cuando se termina de cargar todos los archivos
        this.load.image('background', 'assets/fondos/fondo3.jpg');
        this.load.image('bandeja', 'assets/fondos/fondo1.jpg');

        for (const palo of Carta.PALOS) {
            for (const valor of Carta.VALORES) {
                //console.log(`assets/cartas/${valor}${palo}.png`);
                this.load.image(`carta${valor}${palo}`,`assets/cartas/${valor}${palo}.png`,400,611)
            }            
        }
        this.load.image('cartaJKR','assets/cartas/JKR.png',400,611);
        this.load.image('mazo','assets/cartas/red_back.png',400,611);

    }

}

export default Bootloader;