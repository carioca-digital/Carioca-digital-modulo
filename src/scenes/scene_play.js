import Carta from '../gameObjects/Carta.js';
import Mazo from '../gameObjects/Mazo.js';
import Jugador from '../gameObjects/Jugador.js'
class Scene_play extends Phaser.Scene {

    constructor(){
        super({
            key: "Scene_play"
        });
    }
 // No es necesario poner preload porque la carga de archivos ya lo hace el bootloader

    create(){
        this.jugadores = []
        this.jugadores.push(new Jugador("Lorenzo"));
        this.turno = 0; 
        this.pozo = []
        
        let center_width = this.sys.game.config.width/2;
        let center_height = this.sys.game.config.height/2;
        //Fondos
        this.add.image(center_width,center_height,'background');
        this.bandejaJugador1 = this.add.tileSprite(center_width, center_height*1.8, this.scale.width, this.scale.height/3, 'bandeja');
        this.bandejaJugador2 = this.add.tileSprite(center_width*0.1, center_height*0.7, center_height/5, center_width/1.5, 'bandeja');
        this.bandejaJugador3 = this.add.tileSprite(center_width*1.9, center_height*0.7, center_height/5, center_width/1.5, 'bandeja');
        this.bandejaJugador4 = this.add.tileSprite(center_width, center_height*0.1, center_width/1.5, center_height/5, 'bandeja');
 
        //Zona del pozo
        this.zonaPozo = this.add.zone(center_width, center_height, 140, 170).setRectangleDropZone(140, 170);
        this.zonaPozo.marco = this.add.rectangle(this.zonaPozo.x, this.zonaPozo.y, this.zonaPozo.input.hitArea.width, this.zonaPozo.input.hitArea.height);
        this.zonaPozo.marco.setStrokeStyle(2, 0xffff00);
        
        //this.graphics = this.add.graphics();
        //this.graphics.lineStyle(2, 0xffff00);
        //this.graphics.strokeRect(this.zonaPozo.x - this.zonaPozo.input.hitArea.width / 2, this.zonaPozo.y - this.zonaPozo.input.hitArea.height / 2, this.zonaPozo.input.hitArea.width, this.zonaPozo.input.hitArea.height);
        //Zona de la mano
        this.zonaMano = this.add.zone(center_width, center_height*1.8, this.scale.width/3, this.scale.height/3).setRectangleDropZone(this.scale.width/3, this.scale.height/3);
        this.zonaMano.marco = this.add.rectangle(this.zonaMano.x , this.zonaMano.y , this.zonaMano.input.hitArea.width, this.zonaMano.input.hitArea.height);
        //this.zonaMano.setOrigin(0,0);
        //this.zonaMano.marco.setOrigin(0,0);
        this.zonaMano.marco.setStrokeStyle(2, 0xffff00);

        this.mazo = new Mazo(this).generarMazo().revolverMazo();
        //this.graphics.lineStyle(2, 0xffff00);
        this.mazo.button = this.add.image(center_width-300,center_height,'mazo').setScale(0.15,0.15).setInteractive();
       
        this.mazo.button.on('pointerdown',(pointer)=>{

           let carta = this.mazo.sacarCarta();
           carta.setPosition(pointer.x,pointer.y);
           this.add.existing(carta);
         })
        let self = this;

        this.input.on('dragstart', function (pointer, gameObject) {
            gameObject.setTint(0xff69b4);
            self.children.bringToTop(gameObject);
        },this);

        this.input.on('drag', function (pointer, gameObject, dragX, dragY) {
            gameObject.x = dragX;
            gameObject.y = dragY;
        })
        this.input.on('dragenter', function (pointer, gameObject, dropZone) {
            console.log("entrando a:"+dropZone)
            if(dropZone == self.zonaMano || dropZone == self.zonaPozo)
                dropZone.marco.setStrokeStyle(2, 0x00ffff);
              
        });
        this.input.on('dragleave', function (pointer, gameObject, dropZone) {
            console.log("saliendo de: "+dropZone)

            if(dropZone == self.zonaMano || dropZone == self.zonaPozo)
                dropZone.marco.setStrokeStyle(2, 0xffff00);
            
        });
        this.input.on('drop', function (pointer, gameObject, dropZone) {

            let mano = self.jugadores[self.turno].mano;
            if(dropZone===self.zonaMano && mano.indexOf(gameObject) == -1){
                gameObject.x = (mano.length == 0) ? self.zonaMano.x - self.zonaMano.input.hitArea.width / 2 + 50: mano[mano.length-1].x + 104;
                gameObject.y = dropZone.y;
                mano.push(gameObject);

            }else if(dropZone === self.zonaPozo){

                gameObject.x = dropZone.x//(dropZone.x - 350) + (dropZone.data.values.cards * 50);
                gameObject.y = dropZone.y;
                (mano.indexOf(gameObject)>-1)? mano.splice(mano.indexOf(gameObject),1) : 0;

            }
            self.jugadores[self.turno].mano = mano;
            //gameObject.input.enabled = false;
           // self.socket.emit('cardPlayed', gameObject, self.isPlayerA);
        })
        this.input.on('dragend', function (pointer, gameObject, dropped) {
            let zone = self.zonaPozo;
            gameObject.setTint();
            if(!dropped){
                gameObject.x = gameObject.input.dragStartX;
                gameObject.y = gameObject.input.dragStartY;
            }
            /*
            self.graphics.clear();
            self.graphics.lineStyle(2, 0xffff00);
            self.graphics.strokeRect(zone.x - zone.input.hitArea.width / 2, zone.y - zone.input.hitArea.height / 2, zone.input.hitArea.width, zone.input.hitArea.height);
            */
        })
        

    }
 
}
export default Scene_play;